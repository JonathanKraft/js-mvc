Exercice de Todo List en MVC Js from Scratch pour la promo6 de Simplon Lyon

I- Mise en place du projet (package.json / webpack.config.json)
1. Créer un projet gitlab js-mvc
2. Cloner le projet, puis faire un npm init dedans
3. Installer webpack webpack-cli sass-loader style-loader css-loader en dev-dependencies
4. Installer bootstrap en dependencies
5. Faire la structure de dossier/fichier qui va bien avec un fichier src/js/index.js et un src/index.html
6. Faire la configuration webpack (en copiant collant de celles qu'on a déjà faites, en gros il faut : un point d'entrée, un output, un devtools pour les sources map, un mode development, et la partie qui concerne les loaders qui sera exactement la même que dans le fichier fait la semaine dernière)
7. Rajouter dans les scripts du package.json un script "dev" qui lancera webpack en watch

II- Faire le modèle
1. Créer un fichier todo.js
2. Dans ce fichier, créer une classe Todo et en faire un export (js modulaire toussa), elle aura comme propriété : label en string, checked en boolean (false par défaut) et priority en number
3. Ajouter une méthode check qui fera passer la propriété checked de true à false ou de false à true
4. Créer un fichier model.js
5. Dans ce fichier, créer et exporter une classe Model qui aura en propriété une liste vide (pas d'argument dans le constructeur)
6. Ajouter à cette classe une méthode addTodo qui attendra en paramètre un todo et ajoutera celui ci à la propriété list de la classe
7. Ajouter ensuite une méthode removeTodo qui attendra également un todo en paramètre et fera en sorte de retirer le todo en question de la propriété list de la classe
Plusieurs manières pour ce point : 
* a) avec une boucle for classique, dont on se sert pour parcourir les valeurs actuelles de la list et lorsqu'on tombe sur un todo dont le label correspond à celui de l'argument, on utilise un splice() pour supprimer cet élément
* b) (un poil plus compliquée à saisir) avec la méthode filter sur la list https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter


III. La Vue
1. Créer un fichier view.js et dans ce fichier, faire et exporter une class View
2. La classe view aura une propriété model qui contiendra une instance de la class Model (faire l'instance en paramètre par défaut du constructeur)(modifié)
3. La classe View aura également une méthode display() dont le rôle sera de généré le HTML à partir des données du Modèle (this.model.list)
* a) Rajouter dans le index.html une balise html qui sera le receptacle de notre vue
* b) Dans la méthode, faire une querySelector de cette balise puis remettre son contenu à zéro
* c) Ensuite, faire une boucle for...of sur la list de Todo contenue dans le Model
* d) Dans cette boucle, commencer par juste créer un élément HTML et lui mettre en textContent la valeur du label du Todo actuel
4. Pour tester tout ça, on va dans notre index.js, on fait un import de la classe View puis on en fait une instance et on lui donne comme Model l'instance qu'on avait fait avec nos tests d'hier