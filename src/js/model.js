import { Todo } from "./todo.js";

/**
 * La classe Model représente notre todo list sous forme de données
 * pures, complètement indépendante de l'affichage. Le Model ne doit
 * contenir aucune référence au HTML, au DOM ou quoique ce soit de
 * visuel.
 * On s'en sert ici pour stocker nos Todo dans une liste, mais aussi
 * pour faire des méthodes qui ajouteront ou supprimeront des Todo
 * de la liste en question
 */
export class Model {
    constructor() {
        this.list = [];
    }
    /**
     * Une méthode qui ajoutera un nouveau todo à la liste de l'instance
     * de la classe Model
     * @param {Todo} nouveauTodo le nouveau Todo à ajouter à la liste
     */
    addTodo(nouveauTodo) {
        //On vérifie si la valeur de nouveauTodo est
        //bien une instance de la classe Todo avec instanceof
        if (nouveauTodo instanceof Todo) {
            this.list.push(nouveauTodo);
        }
    }
    /**
     * Une méthode qui supprimera un Todo de la liste de l'instance
     * de la classe Model
     * @param {Todo} rmTodo Le Todo à supprimer de la liste
     */
    removeTodo(rmTodo) {
        //On parcours les éléments du tableau avec un for
        for (let index = 0; index < this.list.length; index++) {
            //On récupère chaque élément du tableua pour le mettre dans
            //une variable item
            let item = this.list[index];
            //On vérifie si le label du Todo donné en argument correspond
            //au label du Todo de la liste sur lequel on est actuellement
            if(rmTodo.label === item.label) {
                //S'ils correspondent, on supprime l'élément en question
                this.list.splice(index, 1);
            }
        }
        /*
        //Deux autres façons de faire la même chose
        this.list = this.list.filter(function(item) {
            return item.label !== rmTodo.label;
        });
        //ici en utilisant une fat arrow function (on en parle bientôt)
        this.list = this.list.filter(item => item.label !== rmTodo.label);
        */
    }
}