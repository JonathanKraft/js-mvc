import '../../node_modules/bootstrap/scss/bootstrap.scss';
import '../scss/style.scss';
import { Todo } from "./todo.js";
import { Model } from "./model.js";
import { View } from "./view.js";

let todoInstance = new Todo('do stuff', 3);

let modelInstance = new Model();

modelInstance.addTodo(todoInstance);
modelInstance.addTodo(new Todo('do other things', 1));
modelInstance.addTodo(new Todo('bloup', 5));

modelInstance.removeTodo(todoInstance);

let viewInstance = new View (modelInstance);
viewInstance.initialization();
