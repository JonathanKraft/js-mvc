import { Model } from "./model.js";
import { Todo } from "./todo.js";

/**
 * Une classe qui représente la partie visuelle / l'affichage de
 * la todo list.
 * Cette classe utilisera une instance de Model qu'elle utilisera
 * pour rajouter/supprimer/accéder à nos todo
 */
export class View {
    constructor(paramModel = new Model()) {
        this.propModel = paramModel;
    }
    /**
     * Cette méthode sert à initialiser la vue : elle va mettre
     * les event listeners sur les éléments HTML et elle va
     * lancer une première fois la méthode display pour afficher
     * l'état initiale de la todo list
     */
    initialization() {
        this.display();

        let form = document.querySelector('#formTodo');
        /*
            on utilise ici ce qu'on appelle une fat arrow function
            C'est un ajout de ECMAScript6, c'est en fait une syntaxe
            différente pour créer des fonctions anonymes.
            Les fat arrow function ont ceci dit une spécificité intéressante :
            elles ne modifient pas la valeur du this.
        */
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            //on récupère les valeurs des inputs du form avec des querySelector
            let inputLabel = document.querySelector('#label').value;
            let inputPriority = document.querySelector('#priority').value;
            //on crée une instance de Todo avec les valeurs du formulaire
            let nouveauTodo = new Todo(inputLabel, inputPriority);
            //on donne cette instance au addTodo de la propriété model
            this.propModel.addTodo(nouveauTodo);
            //on relance la méthode display pour mettre à jour l'affichage
            this.display();

        });

        let todoSection = document.querySelector("section.todos");

        todoSection.addEventListener('change', (event) => {
            let label = event.target.nextSibling.textContent;

            for (let index = 0; index < this.propModel.list.length; index++) {
                let todo = this.propModel.list[index];
                if (todo.label === label) {
                    todo.toggle();
                    break;
                }
            }
        });

    }
    /**
     * La méthode display sert à généré l'affichage HTML de la 
     * todo list à partir du modèle de données.
     */
    display () {
      let section = document.querySelector('.todos');
      section.innerHTML = '';
      /*console.log('ya quoi dans this.propModel.list :');
      console.log(this.propModel.list);*/
      //On fait une boucle sur les todos qui sont stockés dans notre
      //Model qui est dans la propriété propModel de la classe
      for (let itemTodo of this.propModel.list) {
          /*console.log('ya quoi dans todo : ');
          console.log(todo);*/
        let p = this.todoHTML(itemTodo);
        section.appendChild(p);
      }      
    }
    /**
     * Cette méthode sert à généré le HTML d'une instance de Todo.
     * C'est le même principe que les méthodes toHTML() qu'on avait
     * faites dans certaines classes de nos exos précédents
     * @param {Todo} paramTodo Le todo dont on veut faire le HTML
     */
    todoHTML(paramTodo) {
        let div = document.createElement("div");

        let checkbox = document.createElement("input");
        checkbox.setAttribute('type','checkbox');

        let p = document.createElement('p');
        p.textContent = paramTodo.label;

        div.appendChild(checkbox);
        div.appendChild(p);
        return div;
    }
}